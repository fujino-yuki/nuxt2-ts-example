# example

> My nuxt2 example ts project 

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## created project infomation

* **day**  
2018/12/05
* **command for the create**   
```yarn create nuxt-app <my-project>```
* **Option on when create**  
  1. Server => none  
  2. UI => Bootstrap  
  3. mode => spa  
  4. httpReqest => axios  
  5. eslint => no  
  6. prettier => no  

* **Document url for create project**
  * [Qiita: create-nuxt-app から TypeScript の導入](https://qiita.com/TsukasaGR/items/5c911a59875dce87fe42)

* **Additional work**
  ``` bash
  # Insert this code in tsconfig
  "exclude": [
    "node_modules",
    "modules",
    "nuxt.config.js"
  ],
  ```
  ``` bash
  # insert this code in tsconfig
  "strictPropertyInitialization": true,
  ```
