const path = require('path');

module.exports = function() {
  // Add .ts extension for store, middleware and more
  this.nuxt.options.extensions.push("ts")
  // Extend build
  this.extendBuild(config => {
    const tsLoader = {
      loader: "ts-loader",
      options: {
        appendTsSuffixTo: [/\.vue$/]
      }
    }
    const tslintLoader = {
      // loader: 'tslint-loader',
      loader: path.resolve(__dirname, '_tslint-loader.js'),
      exclude: /node_modules|\.nuxt/,
      // linterOptions not work
      // https://github.com/Microsoft/vscode-tslint/issues/325
      enforce: 'pre',
      options: {
        configFile: 'tslint.json',
        formatter: 'grouped',
        formattersDirectory: 'node_modules/custom-tslint-formatters/formatters',
      }
    }
    // Add TypeScript loader
    config.module.rules.push(
      Object.assign(
        {
          test: /((client|server)\.js)|(\.tsx?)$/
        },
        tsLoader,
      )
    )
    // Add TypeScript loaders
    config.module.rules.push(
      Object.assign(
        {
          test: /((client|server)\.js)|(\.tsx?)$/
        },
        tslintLoader,
      )
    )
    // Add TypeScript loader for vue files
    for (let rule of config.module.rules) {
      if (rule.loader === "vue-loader") {
        rule.options.loaders = {
          ts: "tsLoader!tslintLoader"
          // https://github.com/palantir/tslint/issues/2099
        }
      }
    }
    // Add .ts extension in webpack resolve
    if (config.resolve.extensions.indexOf(".ts") === -1) {
      config.resolve.extensions.push(".ts")
    }
  })
}
